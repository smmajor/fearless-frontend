import React, { useState, useEffect } from 'react';

function PresentationForm () {
    const [conferences, setConferences] = useState([])
    const [conference, setConference] = useState("")
    const [company_name, setCompanyName ] = useState("")
    const [presenter_name, setPresenterName] = useState("")
    const [presenter_email, setPresenterEmail] = useState("")
    const [title, setTitle] = useState("")
    const [synopsis, setSynopsis] = useState("")

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value)
    }

    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value)
    }

    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value)
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value)
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value)
    }
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.presenter_name = presenter_name
        data.presenter_email = presenter_email
        data.conference = conference
        data.title = title
        data.synopsis = synopsis
        data.company_name = company_name

        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
         };
        const response = await fetch(presentationUrl, fetchConfig);
         if (response.ok) {
        const newAttendee = await response.json();

      setConference('')
      setPresenterEmail('')
      setCompanyName('')
      setTitle('')
      setSynopsis('')
      setPresenterName('')
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <main>
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} placeholder="Presenter Email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} required id="conference" name="conference" className="form-select">
                  <option>Choose a Conference</option>
                  {conferences.map(conference => {
                            return (
                                <option key={conference.id} value={conference.id}>{conference.name}</option>
                            );
                        })}
                </select>
                </div>
              <div className="form-floating mb-3">
              <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <textarea onChange={handleSynopsisChange} placeholder="Synopsis" className="form-control" required type="text" name="synopsis" id="synopsis" rows="3"></textarea>
                <label htmlFor="synopsis" className="form-label">Synopsis</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </main>

    )
}
export default PresentationForm
