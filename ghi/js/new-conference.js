window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(url);
        console.log(response)
        if (!response.ok) {
            // when the response is bad
            throw new Error('Network response was bad');
          } else {
            const data = await response.json();
            const selectTag = document.getElementById('location')
            for (let location of data.locations) {
                const newOption = document.createElement('option');
                newOption.innerHTML = location.name
                newOption.value = location.id
                selectTag.appendChild(newOption)
          }
          }
          }catch (e) {
            // if an error is raised
            console.error('There was an error fetching the state data', e)
          }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                  'Content-Type': 'application/json',
                },
              };
              const response = await fetch(conferenceUrl, fetchConfig);
              if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
              }
        });
    });
