window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        console.log(response)
        if (!response.ok) {
            // when the response is bad
            throw new Error('Network response was bad');
          } else {
            const data = await response.json();
            const selectTag = document.getElementById('conference')
            for (let conference of data.conferences) {
                const newOption = document.createElement('option');
                newOption.innerHTML = conference.name
                newOption.value = conference.id
                selectTag.appendChild(newOption)
          }
          }
          }catch (e) {
            // if an error is raised
            console.error('There was an error fetching the conference data', e)
          }
            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const selectTag = document.getElementById('conference')
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const confObject = JSON.parse(json)
            const presentationUrl = `http://localhost:8000/api/conferences/${confObject.conference}/presentations/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                  'Content-Type': 'application/json',
                },
              };
              const response = await fetch(presentationUrl, fetchConfig);
              if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
              }
        });
    });
